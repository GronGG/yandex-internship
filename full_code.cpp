// Problem: write an ideal hash table realization.
// Input: an array of non-repeating integers with absolute value under 10^9
// Given a query decide whether it's present in hash table.


#include <iostream>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <limits>
#include <cstdio>

using std::vector;
using std::cout;
using std::cin;

// using templates in defining mod() function for all combinations
// of argument types: long long int <-> int, long long int <-> long long int, etc...
// defining mod() function for correct processing of negative dividend

// returns: divident (mod divisor)
template<typename T1T, typename T2T>
size_t mod(T1T dividend, T2T divisor) {
    if (dividend >= 0) {
        return dividend % divisor;
    }
    return dividend % divisor + divisor;
}


class UniversalHashFunction {
// (multiplier * key + increment) mod divisor
    static const int divisor = 2111623773;
    int multiplier;
    int increment;

public:
    UniversalHashFunction() {
        multiplier = mod(rand(), divisor) + 1;
        increment = mod(rand(), divisor);
    }

    size_t operator()(int key) const {
        long long int hash =
            static_cast<long long int>(multiplier)
            * static_cast<long long int>(key)
            + static_cast<long long int>(increment);
        hash = mod(hash, static_cast<long long int>(divisor));
        return hash;
    }
};


class IdealHashTable {

    UniversalHashFunction hash_function;
    vector<int> table;

    bool fill_table_with_elements(const vector<int>& init_vector) {
        table.assign(init_vector.size() * init_vector.size(), EMPTY);
        hash_function = UniversalHashFunction();
        for (size_t index = 0; index < init_vector.size(); ++index) {
            size_t hash_value = hash_function(init_vector[index]);
            if (table[hash_value % table.size()] != EMPTY) { // collision!
                return false;
            }
            table[hash_value % table.size()] = init_vector[index];
        }
        return true;
    }

public:
    static const int EMPTY = 1000000009;

    void initialize(const vector <int>& init_vector) {
        while (!fill_table_with_elements(init_vector)) {}
    }

    bool contains(int key) const {
        if (table.empty()) {
            return false;
        }
        size_t hash_value = hash_function(key);
        return (table[hash_value % table.size()] == key);
    }
};

// Instantiating a member to avoid linking error
const int IdealHashTable::EMPTY;

class FixedSet {

    UniversalHashFunction first_level_hash;


    vector<IdealHashTable> buckets;


    void select_hash_function(const vector<int>& items) {
        const size_t LENGTH_CRITERION_MULTIPLIER = 3;

        vector<size_t> lengths;
        unsigned long long total_length;
        do {
            first_level_hash = UniversalHashFunction();

            lengths.assign(items.size(), 0);
            for (int item : items) {
                ++lengths[first_level_hash(item) % lengths.size()];
            }

            total_length = 0;
            for (size_t length : lengths) {
                total_length += length * length;
            }
        } while (total_length > LENGTH_CRITERION_MULTIPLIER * items.size());
    }

    vector< vector<int> > build_second_level_lists(const vector<int>& init_vector) const {
        vector< vector<int> >second_level_lists(buckets.size());
        for (size_t index = 0; index < buckets.size(); ++index) {
            size_t hash_value = first_level_hash(init_vector[index]);
            second_level_lists[hash_value % buckets.size()]
                .push_back(init_vector[index]);
        }

        return second_level_lists;
    }

    void match_second_level_hashes(const vector< vector<int> >& second_level_lists) {
        for (size_t list_index = 0; list_index < buckets.size(); ++list_index) {

            if (second_level_lists[list_index].empty()) {
                continue;
            }

            buckets[list_index]
                .initialize(second_level_lists[list_index]);
        }
    }

public:
    void initialize(const vector<int>& init_vector) {
        buckets = vector<IdealHashTable>(init_vector.size());
        select_hash_function(init_vector);
        vector< vector<int> >second_level_lists
            = build_second_level_lists(init_vector);
        match_second_level_hashes(second_level_lists);
    }



    bool contains(int number) const {
        int first_index = first_level_hash(number) % buckets.size();
        return buckets[first_index].contains(number);
    }
};


vector<int> input_initialize_vector() {
    int set_size;
    scanf("%d", &set_size);
    vector<int> set_vector(set_size);

    for (int i = 0; i < set_size; ++i) {
        scanf("%d", &(set_vector[i]));
    }

    return set_vector;
}

void process_queries(const FixedSet& fixed_set) {
    int query_length;
    scanf("%d", &query_length);
    for (int index = 0; index < query_length; ++index) {
        int query;
        scanf("%d", &query);
        if (fixed_set.contains(query)) {
            printf("Yes\n");
        } else {
            printf("No\n");
        }
    }
}


int main()
{

    srand(20);

    const vector<int> initialize_vector = input_initialize_vector();

    FixedSet fixed_set;
    fixed_set.initialize(initialize_vector);

    process_queries(fixed_set);
    return 0;
}
